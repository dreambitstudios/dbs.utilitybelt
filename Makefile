#!/usr/bin/make

#
# envirment setup
#

# virtualenv related stuff
VIRTUALENV_PROMPT = dbs.utilitybelt

# python executables
PYTHON := $(shell if [ ! -z "`python --version 2>&1 | grep 'Python 2'`" ] ; then echo python; else echo python2; fi)
VIRTUAL_PYTHON = virtual/bin/python
PIP = virtual/bin/pip

# testing
APPS_TO_TEST = dbs.utilitybelt
COVERAGE = virtual/bin/coverage

#
# default
#

.PHONY: all run virtual construct-development tags clean clean-pyc test doc doc-crawl build-test-egg deploy-test

all: construct-development

help:
	@echo "Please use \`make <target>' where <target> is one of"
	@echo ""
	@echo "  <empty>                create development enviroment"
	@echo "  virtual                create virtual enviroment"
	@echo "  construct-development  creates virtual enviroment and installs dependencies"
	@echo "  tags                   recuresavely runs ctags on src folder"
	@echo "  clean                  deletes files created by using/installing application"
	@echo "  clean-pyc              deletes only *.pyc files"
	@echo "  test                   Not implemented yet"
	@echo "  doc                    crawls src file in order to create documentation source files and generates HTML"
	@echo "  doc-crawl              crawls src file and generates .rst source files for documentation"
	@echo "  build-test-egg         generates installable egg with git short hash added to version number"

#
# targets
#

virtual:
	virtualenv --unzip-setuptools --prompt='$(VIRTUALENV_PROMPT)::' --python=$(PYTHON) virtual \
	|| \
	virtualenv --unzip-setuptools --python=$(PYTHON) virtual

#
# Development
#

construct-development: virtual tags
	$(VIRTUAL_PYTHON) bootstrap.py
	virtual/bin/buildout -N

# Only if you have ctags intalled
tags:
	ctags -R src/*

#
# Helpers
#

clean: clean-pyc
	rm -rf virtual
	rm -rf develop-eggs
	rm -rf build
	rm -rf dist
	rm tags
	rm .installed.cfg
	find . -name "*.egg-info" -exec rm -rf '{}' \;

clean-pyc:
	find . -name "*.pyc" -exec rm -rf '{}' \;

test:
	echo "TODO"

doc: doc-crawl
	make html -C doc

doc-crawl:
	virtual/bin/sphinx-apidoc -o doc/source/generated src

#
# Testing enviroment
#

build-test-egg:
	$(VIRTUAL_PYTHON) setup.py egg_info \
		-b`git log --pretty=format:'_%h' -n 1`_DEV \
		sdist rotate -m.tar.gz -k1

build-egg:
	$(VIRTUAL_PYTHON) setup.py sdist

upload:
	python setup.py sdist upload -r pypi
